﻿using System;
using UnityEngine;

[Serializable]
public class MineSave : Save
{
    public Data data;
    private Mine mine;
    private string jsonString;

    [Serializable]
    public class Data : BaseData
    {
        //the data that will get saved.
        public Vector3 position;
        public Vector3 scale;    
        public bool exploded;
        public float timer;
        public Color color;

    }

    void Awake()
    {
        mine = GetComponent<Mine>();
        data = new Data();
    }


    //serializing the data into a json
    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = mine.transform.position;
        data.scale = mine.transform.localScale;
        data.exploded = mine.exploded;
        data.timer = mine.timer;
        data.color = mine.color;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //getting the data from a json when loaded
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        mine.transform.position = data.position;
        mine.transform.localScale = data.scale;
        mine.exploded = data.exploded;
        mine.timer = data.timer;
        mine.color = data.color;
        mine.transform.parent = GameObject.Find("Mines").transform;
    }
}
