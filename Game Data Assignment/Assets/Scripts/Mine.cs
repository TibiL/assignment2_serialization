﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public LayerMask tank;
    public bool exploded;
    public float timer;
    public Color color;

    public void Start()
    {
        //sets the color of the mine at the start
        ChangeColor();
    }


    //checks to see if the mine needs to change color or size.
    private void Update()
    {
        if (exploded && timer <=2f)
        {
            timer += Time.deltaTime;
            ChangeColor();
        }
    }


    //triggers the explosion on collision and makes the tank respawn if caught in explosion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!exploded)
        {
          
            exploded = true;
            ChangeColor();
        }

        else
        {
            if (collision.gameObject.name == "Tank" && timer <= 2f && timer >=1.5f)
            {
                collision.transform.position = Vector3.zero;
            }
        }
    }



    //changes the colors and size of the mine depending on if it was triggered or not.
    public void ChangeColor()
    {
      
        //green color if it was not triggered
        if (!exploded)
        {
            color = new Color32(46, 142, 29, 255);
            transform.localScale = Vector3.one;
        }

        //different colors and sizes depending on when it was triggered
        else if (timer < 1.5)
        {
            color = new Color32(255, 130 ,0 , 255);
            transform.localScale = Vector3.one;
        }
        else if (timer < 1.75f)
        {
            color = Color.Lerp(new Color32(255, 0, 0, 255) , new Color32(255, 130, 0, 255),  (1.75f - timer) / 0.25f);

            transform.localScale = Vector3.one * Mathf.Lerp(6, 1,  (1.75f - timer) / 0.25f);
        }
        else if(timer < 2f)
        {
            color = Color.Lerp(new Color32(0, 0, 0, 255), new Color32(255, 0, 0, 255), (2f - timer) / 0.25f);

            transform.localScale = Vector3.one * Mathf.Lerp(0, 6, (2f - timer) / 0.25f);
        }

        //black color and shrinks to dissapear if it already exploded.
        else
        {
            color = new Color32(0, 0, 0, 255);
            transform.localScale = Vector3.zero;
            Destroy(this.gameObject);
        }

       //updates the sprite with the right color.
        GetComponentInChildren<SpriteRenderer>().color = color;
    }
}
